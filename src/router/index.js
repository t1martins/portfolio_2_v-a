import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home.vue'

Vue.use(VueRouter)

//all routes called here
  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/paintings',
    name: 'Paintings',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/paintings.vue')
  }, 
  {
    path: '/prints',
    name: 'Prints',
    component: ()=> import('../views/prints.vue')
  }, 
  {
    path: '/results',
    name: 'Results',
    component: ()=>('../components/result.vue')
  }, 
  {
    //item id from object numnber. used in anItem function
    path: '/item/:id',
    name: 'Item',
    props: true,
    component: () => import('../views/item.vue')
  },
  {
    path: '/eighteen',
    name: 'eighteen',
    props: true,
    component: () => import('../views/1800.vue')
  },
  {
    path: '/nineteen',
    name: 'nineteen',
    props: true,
    component: () => import('../views/1900.vue')
  },
  {
    path: '/twenty',
    name: 'twenty',
    props: true,
    component: () => import('../views/2000.vue')
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
}
})

export default router
